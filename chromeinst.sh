#!/bin/bash
mkdir $1
cd $1
FILE=$(curl -s https://api.github.com/repos/ungoogled-software/ungoogled-chromium-portablelinux/releases/latest | jq .assets | jq -r .[1].browser_download_url)
curl -Lo chrome.tar.xz $FILE  
tar -xpvf chrome.tar.xz
cd ungoogled-chromium*
mkdir ~/.local/share/applications
echo -e "[Desktop Entry]\nCategories=Network;WebBrowser;\nEncoding=UTF-8\nExec=$(pwd)/chrome --password-store=basic %U\nName[ru]=Хромиум\nName=Chromium\nType=Application\nTerminal=false\nIcon=chromium" > ~/.local/share/applications/ung_chromium.desktop
echo "Chromium is installed"
