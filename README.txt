Useful if you don’t like flatpack and the repositories don’t have Ungoogled Chromium

Dependencies: curl, jq

Usage:
bash chromeinst.sh ~/<location of installation>
